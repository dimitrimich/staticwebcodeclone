package com.amdocs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    @Test
    public void testAdd(){
        final Calculator calc = new Calculator();
        final int result = calc.add();

        assertEquals("testing addition ", 9, result);

    }
    @Test
    public void testSubtract(){
        final Calculator calc = new Calculator();
        final int result = calc.sub();

        assertEquals("testing subtraction ", 3, result);

    }
}
