package com.amdocs;

import com.amdocs.Increment;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IncrementTest {

    @Test
    public void testDecreaseCounterZero(){
        Increment incr = new Increment();
        int returnValue = incr.decreasecounter(0);

        assertEquals("Counter check with 0", 0, returnValue);

    }
    @Test
    public void testDecreaseCounterOne(){
        Increment incr = new Increment();
        int returnValue = incr.decreasecounter(1);

        assertEquals("Counter check with 1",returnValue, 1);

    }

    @Test
    public void testDecreaseCounterBiggerThanOne(){
        Increment incr = new Increment();
        int returnValue = incr.decreasecounter(2);

        assertEquals("Counter check with 2", 2, returnValue);

    }
}
