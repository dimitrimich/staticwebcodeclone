package com.amdocs;

public class Increment {
	
	private int counter = 1;

	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		final int returnValue;
		if (input == 0){
			returnValue = --counter;
		}
		else {
			if (input == 1) {
				returnValue = counter;
			}
			else{
				returnValue = ++counter;
			}
		}
		return returnValue;
	}			
}